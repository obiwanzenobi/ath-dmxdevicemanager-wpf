﻿using ATHDmxDevicesManager.Models;
using ATHDmxDevicesManager.VModels;
using MaterialDesignThemes.Wpf;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ATHDmxDevicesManager
{

    public partial class MainWindow : Window //also as view model
    {
        public const string SERVER_ADDRESS = "http://dmxmagic.somee.com/api/";
        //public const string SERVER_ADDRESS = "http://localhost:59294/api/";
        public const string DEVICE_ENDPOINT_PATH = "devices";

        private MainWindowViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            this._viewModel = new MainWindowViewModel(new RestService());
            this.DataContext = _viewModel;

            this._viewModel.InitData();
        }

        private void NumericValidation(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SelectionChange(Device selectedDevice)
        {
            gridUpdate.DataContext = selectedDevice;
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.CreateDevice();
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.UpdateDevice();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.RemoveDevice();
        }
    }
}
