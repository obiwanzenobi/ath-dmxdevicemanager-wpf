﻿using ATHDmxDevicesManager.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ATHDmxDevicesManager.VModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public CreateUpdateSectionViewModel CreateViewModel { get; set; }
        public CreateUpdateSectionViewModel UpdateViewModel { get; set; }

        private RestService _restService;

        public MainWindowViewModel(RestService restService)
        {
            _restService = restService;

            InitState();
        }

        private void InitState()
        {
            CreateViewModel = new CreateUpdateSectionViewModel();
            UpdateViewModel = new CreateUpdateSectionViewModel();
            _devices = new List<Device>();
            _device = new Device();
        }

        private bool _loaderVisible;
        public bool LoaderVisible
        {
            get
            {
                return _loaderVisible;
            }

            set
            {
                _loaderVisible = value;
                OnPropertyChanged("LoaderVisible");
            }
        }

        private List<Device> _devices;
        public List<Device> Devices
        {
            get
            {
                return _devices;
            }
            set
            {
                _devices = value;
                OnPropertyChanged("Devices");
            }
        }

        private Device _device;
        public Device SelectedDevice
        {
            get { return _device; }
            set
            {
                if (value == null)
                {
                    return;
                }

                _device = value;
                UpdateViewModel.SetDevice(_device);
                OnPropertyChanged("SelectedDevice");
            }
        }

        public void InitData()
        {
            FetchData();
        }

        private void FetchData()
        {
            LoaderVisible = true;
            _restService.FetchData(response =>
            {
                Devices = response.Data;
                LoaderVisible = false;
            });

        }

        public void CreateDevice()
        {
            LoaderVisible = true;
            _restService.UploadDevice(CreateViewModel.GetDevice(), response =>
                                {
                                    LoaderVisible = false;
                                    FetchData();
                                }
                         );
        }

        public void UpdateDevice()
        {

            LoaderVisible = true;

            Device device = UpdateViewModel.GetDevice();
            device.ID = SelectedDevice.ID;

            _restService.Update(device, response =>
            {
                LoaderVisible = false;
                FetchData();
            }
            );

        }

        public void RemoveDevice()
        {
            LoaderVisible = true;

            Device device = CreateViewModel.GetDevice();
            device.ID = SelectedDevice.ID;

            _restService.UploadDevice(device, response =>
                {
                    LoaderVisible = false;
                    FetchData();
                }
            );
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
