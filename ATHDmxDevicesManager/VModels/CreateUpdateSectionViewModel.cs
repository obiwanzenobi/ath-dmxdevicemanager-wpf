﻿using ATHDmxDevicesManager.Interfaces;
using ATHDmxDevicesManager.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ATHDmxDevicesManager.VModels
{
    public class CreateUpdateSectionViewModel : INotifyPropertyChanged
    {

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _manufacturer;
        public string Manufacturer
        {
            get
            {
                return _manufacturer;
            }
            set
            {
                _manufacturer = value;
                OnPropertyChanged("Manufacturer");
            }
        }

        private string _powerConsumption;
        public string PowerConsumption
        {
            get
            {
                return _powerConsumption;
            }
            set
            {
                _powerConsumption = value;
                OnPropertyChanged("PowerConsumption");
            }
        }

        private bool _isLed;
        public bool IsLed
        {
            get
            {
                return _isLed;
            }
            set
            {
                _isLed = value;
                OnPropertyChanged("IsLed");
            }

        }

        private string _channels;
        public string Channels
        {
            get
            {
                return _channels;
            }
            set
            {
                _channels = value;
                OnPropertyChanged("Channels");
            }
        }

        public Device GetDevice()
        {
            return new Device()
            {
                PowerConsumption = int.Parse(_powerConsumption),
                Name = _name,
                Manufacturer = _manufacturer,
                Led = _isLed,
                Channels = int.Parse(_channels)
            };
        }

        public void SetDevice(Device device)
        {
            PowerConsumption = device.PowerConsumption.ToString();
            Name = device.Name;
            Manufacturer = device.Manufacturer;
            IsLed = device.Led;
            Channels = device.Channels.ToString();

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
