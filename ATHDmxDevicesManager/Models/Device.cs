﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATHDmxDevicesManager.Models
{
    public class Device
    {
        public long ID { get; set; }
        public string Manufacturer { get; set; }
        public string Name { get; set; }
        public int Channels { get; set; }
        public bool Led { get; set; }
        public int PowerConsumption { get; set; }
    }
}
