﻿using ATHDmxDevicesManager.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATHDmxDevicesManager
{
    public class RestService
    {

        public const string SERVER_ADDRESS = "http://dmxmagic.somee.com/api/";
        //public const string SERVER_ADDRESS = "http://localhost:59294/api/";
        public const string DEVICE_ENDPOINT_PATH = "devices";

        private RestClient _client;

        public RestService()
        {
            _client = new RestClient(SERVER_ADDRESS);
        }

        public void Update(Device device, Action<IRestResponse> action)
        {
            RestRequest updateRequest = new RestRequest(DEVICE_ENDPOINT_PATH + "/{id}", Method.PUT);

            updateRequest.AddJsonBody(device);
            updateRequest.AddUrlSegment("id", device.ID.ToString());

            _client.ExecuteAsync<List<Device>>(updateRequest, action);
        }

        public void FetchData(Action<IRestResponse<List<Device>>> action)
        {
            RestRequest request = new RestRequest(DEVICE_ENDPOINT_PATH, Method.GET);
            _client.ExecuteAsync(request, action);
        }

        public void UploadDevice(Device device, Action<IRestResponse> action)
        {
            RestRequest createRequest = new RestRequest(DEVICE_ENDPOINT_PATH, Method.POST);
            createRequest.AddJsonBody(device);

            _client.ExecuteAsync(createRequest, action);
        }

        public void RemoveDevice(Device device, Action<IRestResponse> action)
        {
            RestRequest updateRequest = new RestRequest(DEVICE_ENDPOINT_PATH + "/{id}", Method.DELETE);

            updateRequest.AddJsonBody(device);
            updateRequest.AddUrlSegment("id", device.ID.ToString());

            _client.ExecuteAsync<List<Device>>(updateRequest, action);
        }
    }
}
